# Odoo Openshift source Docker images

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

## run

```bash
sudo docker run -d -e POSTGRES_USER=odoo -e POSTGRES_PASSWORD=odoo -e POSTGRES_DB=postgres --name db12 postgres:12.3
sudo docker run -p 8013:8069 --name web13 --link db12:db -t nutimvn/min:13.0
```

# build
```bash
sudo docker build -t nutimvn/min:13.0 . --squash
#sudo docker tag cc963b2f8e9a nutimvn/max:13.0
sudo docker login
sudo docker push nutimvn/min:13.0

# Bao gom:

  - Non-root
  - Openshift!

## Chay thu

```sh
oc new-project odoo && oc create -f odoo-template-openshift.yaml
```


FROM python:3.7.2-alpine
LABEL autor="NUTIM <hi@nutim.vn>" \
  io.k8s.description="Odoo s2i images" \
  io.k8s.display-name="Odoo python" \
  io.openshift.tags="builder,python,s2i,odoo"

## Home and workdir
ENV APP_ROOT=/opt/app-root
WORKDIR ${APP_ROOT}
## Source odoo
ADD odoo ${APP_ROOT}

## Install system and apps dependencies
RUN \
 apk add --no-cache postgresql-libs g++ libxslt-dev openldap-dev lsof && \
 apk add --no-cache --virtual .build-deps gcc musl-dev postgresql-dev vim && \
 apk --update add libxml2-dev libxslt-dev libffi-dev gcc musl-dev libgcc openssl-dev curl && \
 apk add jpeg-dev zlib-dev freetype-dev lcms2-dev openjpeg-dev tiff-dev tk-dev tcl-dev && \
 pip install --upgrade pip && \
 python3 -m pip install -r requirements.txt --no-cache-dir && \
 apk --purge del .build-deps

## OPTIONAL: If you want to install openhrms_core plugin (takes a HUGE amount of time installing pandas)
#RUN pip install numpy && pip install pandas
#RUN wget https://apps.odoo.com/loempia/download/ohrms_core/12.0.1.0.0/6czNBZ0bGreJgEGKl5GScy.zip?deps -P ${APP_ROOT}/addons
#RUN unzip ${APP_ROOT}/addons/6*.zip* -d ${APP_ROOT}/addons

RUN mkdir -p /run/nginx

## Create a non-root user
ENV PATH=${APP_ROOT}/bin:${PATH} HOME=${APP_ROOT}
COPY s2i/bin/ ${APP_ROOT}/bin/
RUN chmod -R u+x ${APP_ROOT}/bin && \
    chgrp -R 0 ${APP_ROOT} && \
    chmod -R g=u ${APP_ROOT} /etc/passwd && \
    chmod -R g=u /run/nginx

USER 1001   
CMD ["bin/run"]
